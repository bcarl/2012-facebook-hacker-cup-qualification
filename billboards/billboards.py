#!/usr/bin/env python

import sys

def max_font_size(width, height, words):
	# check constraints of input
	if width < 1 or width > 1000:
		return 0
	if height < 1 or height > 1000:
		return 0
	
	if height == 1 and width < len(' '.join(words)): # can't fit whole string on one line
		return 0

	area = width * height
	word_areas = []
	for word in words:
		word_areas.append((len(word), 1)) # (w,h)
	
	if width < max(word_areas): # can't fit the biggest word on one line
		return 0
	
	max_size = width / max(word_areas)
	
	while max_size > 0:
		sized_word_areas = []
		for word_area in word_areas:
			sized_word_areas.append((max_size * word_area[0], max_size)) # (w,h)
		agg_height = 0
		for (w,h) in sized_word_areas:
			if w > width:
				continue # Go smaller...
			agg_height += h
		if agg_height > height:
			continue # Go smaller...
		
	
	word1 = words.pop(0)
	length = len(word1)
	size = width / length
	
	word2 = words.pop(0)
	line_width = 0
	# while line_width < width:
	# 	pass
	

def main(input):
	lines = input.split('\n')
	count = int(lines[0])
	
	# check constraints of input
	if count < 1 or count > 20:
		sys.exit("bad input: bad T")
		
	billboards = lines[1:]
	if len(billboards) != count:
		sys.exit("bad input: T does not match lines of input")
	
	i = 0
    while i < count:
		whs = billboard.split(' ')
		print 'Case #%d: %d' % (i+1, max_font_size(int(whs[0]), int(whs[1]), whs[2:]))
		i += 1
	


if __name__ == '__main__':
	if len(sys.argv) != 2:
		sys.exit("Wrong number of args.")
	fp = sys.argv[1]
	main(open(fp).read())
