#!/usr/bin/env python

import sys

def how_many_hackercups(sentence):
    letters = list(sentence)
    hackercups = 0
    try:
        while True:
            for letter in 'HACKERCUP':
                letters.remove(letter)
            hackercups += 1
    except:
        pass
    return hackercups

def main(input):
    lines = input.split('\n')
    count = int(lines[0])
    
    # check constraints of input
    if count <= 1 or count > 20:
        sys.exit("bad input: bad T")
        
    sentences = lines[1:]
    
    i = 0
    while i < count:
        print 'Case #%d: %d' % (i+1, how_many_hackercups(sentences[i]))
        i += 1

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("Wrong number of args.")
    fp = sys.argv[1]
    main(open(fp).read())
