#!/usr/bin/env python

import sys

def deals_and_bargains(n, p, w, m, k, a, b, c, d):
    n = int(n)
    p = int(p)
    w = int(w)
    m = int(m)
    k = int(k)
    a = int(a)
    b = int(b)
    c = int(c)
    d = int(d)
    
    i = 0
    while i < n:
        print p,w
        p = ((a*p + b) % m) + 1
        w = ((c*w + d) % k) + 1
        i += 1
    return (0,0)

def main(input):
    lines = input.split('\n')
    count = int(lines[0])
    
    # check constraints of input
    if count < 1 or count > 20:
        sys.exit("bad input: bad T")
    
    # Get rid of first line (count)
    lines.pop(0)
    
    i = 0
    while i < count:
        line = lines.pop(0)
        args = line.split(' ')
        print 'Case #%d: ' % (i+1) + '%d %d' % deals_and_bargains(*args)
        i += 1

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("Wrong number of args.")
    fp = sys.argv[1]
    main(open(fp).read())
